# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "simport"
  spec.version       = "1.7.0"
  spec.authors       = ["Redhwan Nacef"]
  spec.email         = ["nacefredhwan@gmail.com"]

  spec.summary       = "A simple portfolio and blog theme for jekyll."
  spec.homepage      = "https://gitlab.com/redhwannacef/simport"
  spec.license       = "MIT"

  spec.metadata["plugin_type"] = "theme"

  spec.files = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.8", ">= 3.7"
  spec.add_runtime_dependency "jekyll-feed", "~> 0.12.1"
  spec.add_runtime_dependency "jekyll-seo-tag", "~> 2.6", ">= 2.6.1"
  spec.add_runtime_dependency "jekyll-sitemap", "~> 1.3", ">= 1.3.1"
  spec.add_runtime_dependency "jekyll-remote-theme", "~> 0.4.0"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12.0"
end
