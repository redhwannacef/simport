# simport

A **sim**ple **por**tfolio and blog theme for jekyll.

## Getting Started

Create a new directory and run the `jekyll new {site name}` command. 

Add the following to your Jekyll site's `Gemfile` (see below for explanation on the plugins):

```ruby
gem 'simport', '~> 1.3' # check here for the latest version: https://rubygems.org/gems/simport
```

Then add this line to your Jekyll site's `_config.yml`. (The plugins are automatically added by the theme):

```yaml
plugins:
  - jekyll-feed
  - jekyll-sitemap
  - jekyll-seo-tag
  - jekyll-remote-theme
remote_theme: redhwannacef/simport
```

Finally add an `index.md` with the following:
```
---
layout: home
---
```

And then execute:

    $ bundle
    $ bundle exec jekyll serve

## Usage

### Homepage

To get started you must have a homepage. This is done with by adding either an `index.md` or `index.html` file in the root directory. There is a home layout below that was designed for this page. 

### Adding a logo

There is a default logo in the navbar at the top left of the page. This can be overridden by creating an `_includes/logo.html` file.

### Adding a top level page

You can add a top level page with two steps.

1. Add a navigation button: create a `_data/navigation.yml` file. This is in the format of a list with a name (to be displayed) and a link to the page. For example:
```
- name: About
  link: /about/
```

2. Add the page:  create a `_pages/{link}.md` file, where {link} is the link defined above without the forward slashes `/`. In this example it would be `_pages/about.md`. Pick a layout, add a permalink with the same value as the link and optionally add a title for the page. E.g:
```
---
layout: home
permalink: '/about/'
title: 'About'
---
Hello World!
```

### Layouts

#### home

This layout is designed for the homepage. Simply an centered area for introduction text.

#### blog/post

This is a standard blog layout, listing all blog posts, date and excerpt (the first paragraph) found in the `_posts`. The posts should be name in the format `YYYY-MM-DD-{name}.md` for example `2018-08-20-bananas.md`. The posts should use the `posts` layout with the following liquid variables: title, author and date. For example: 
```
---
layout: post
title:  Bananas
author: Alice
date: 2018-08-20
---
```
#### tils/posts

This the same as the blogs layout but looks for posts in the `_tils` directory. The name of this file does not need to include the date. The posts in here should be in the same format as the blogs with one exception, there will need to be an additional variable `excerpt` with the desired excerpt in the post. For example:
```
---
layout: post
title:  Bananas
author: Alice
date: 2018-08-20
excerpt: An intro to bananas
---
```

Note: By default jekyll knows to look for blog/posts. For TILs, one additionaly configuration is required. Add the following to the `_config.yml`:
```
collections:
  tils:
    output: true
```

#### page

This is the layout that includes the navbar. This has no styling and can be used for any cusom pages.

## Plugins

These three official plugins are useful Jekyll sites and are required to get started:

[jekyll-sitemap](https://github.com/jekyll/jekyll-sitemap) - Creates a sitemap file to help search engines index content

[jekyll-feed](https://github.com/jekyll/jekyll-feed) - Creates an RSS feed for your posts

[jekyll-seo-tag](https://github.com/jekyll/jekyll-seo-tag) - Adds meta tags to help with SEO

## Enabling Google Analytics

To enable Google Analytics, add the following lines to your Jekyll site:

```
google_analytics: UA-NNNNNNNN-N
```

Google Analytics will only appear in production, i.e., JEKYLL_ENV=production

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/hello. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be bundled.
To add a custom directory to your theme-gem, please edit the regexp in `simport.gemspec` accordingly.

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

